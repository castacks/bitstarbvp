#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/jay/simulation_ws/src/bitstar3d/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/jay/simulation_ws/src/bitstar3d/devel/lib:$LD_LIBRARY_PATH"
export PATH="/opt/ros/kinetic/bin:/home/jay/Documents/clion-2019.1.4/bin:/home/jay/bin:/home/jay/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/jay/EMBARK/embark/src/embark_docker/docker_scripts/scripts/"
export PKG_CONFIG_PATH="/home/jay/simulation_ws/src/bitstar3d/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/jay/simulation_ws/src/bitstar3d/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/jay/simulation_ws/src/bitstar3d:/home/jay/simulation_ws/src/bitstarbvp:/home/jay/simulation_ws/src/bitstarT:$ROS_PACKAGE_PATH"